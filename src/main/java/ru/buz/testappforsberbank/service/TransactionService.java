package ru.buz.testappforsberbank.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.buz.testappforsberbank.db.models.Account;
import ru.buz.testappforsberbank.db.models.Transaction;
import ru.buz.testappforsberbank.db.repository.AccountRepository;
import ru.buz.testappforsberbank.db.repository.TransactionLogRepository;
import ru.buz.testappforsberbank.request.AccountRequest;
import ru.buz.testappforsberbank.response.AccountResponse;

import java.math.BigDecimal;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class TransactionService {
    private final AccountRepository accountRepository;
    private final TransactionLogRepository transactionLogRepository;


    @Transactional
    public void transaction(Long id, BigDecimal sum) {
        Account account = accountRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Can't find account with id " + id));

        accountRepository.save(account
                .setCurrentBalance(account
                        .getCurrentBalance()
                        .add(sum)));

        transactionLogRepository.save(new Transaction()
                .setAccount(account)
                .setDate(new Date())
                .setTransactionSum(sum));
    }

}
