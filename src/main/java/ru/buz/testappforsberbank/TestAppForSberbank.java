package ru.buz.testappforsberbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestAppForSberbank {

    public static void main(String[] args) {
        SpringApplication.run(TestAppForSberbank.class, args);
    }

}

