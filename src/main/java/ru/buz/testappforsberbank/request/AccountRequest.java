package ru.buz.testappforsberbank.request;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class AccountRequest {
    private Long id;
    private BigDecimal sumOfTransaction;
}
