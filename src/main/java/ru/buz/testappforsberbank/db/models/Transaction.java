package ru.buz.testappforsberbank.db.models;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Accessors(chain = true)
@Entity
@Table(name = "transaction_log")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_account", referencedColumnName = "id")
    private Account account;

    @Column(name = "date_time")
    private Date date;

    @Column(name = "transaction_sum")
    private BigDecimal transactionSum;

}
