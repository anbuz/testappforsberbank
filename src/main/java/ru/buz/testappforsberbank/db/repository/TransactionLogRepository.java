package ru.buz.testappforsberbank.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.buz.testappforsberbank.db.models.Transaction;

public interface TransactionLogRepository extends JpaRepository<Transaction,Long> {
}
