package ru.buz.testappforsberbank.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.buz.testappforsberbank.db.models.Account;
import ru.buz.testappforsberbank.db.repository.AccountRepository;
import ru.buz.testappforsberbank.db.repository.TransactionLogRepository;
import ru.buz.testappforsberbank.request.AccountRequest;
import ru.buz.testappforsberbank.response.AccountResponse;
import ru.buz.testappforsberbank.response.TransactionResponse;
import ru.buz.testappforsberbank.service.TransactionService;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
@RestController
@RequestMapping("/accounts")
public class AccountController {
    private final TransactionLogRepository logRepository;
    private final AccountRepository accountRepository;

    @Autowired
    private TransactionService transactionService;

    private static Object apply(Account account) {
        return new AccountResponse().setId(account.getId()).setCurrentBalance(account.getCurrentBalance());
    }

    @GetMapping
    public @ResponseBody
    List<AccountResponse> getAccounts() {
        return accountRepository.findAll()
                .stream()
                .map(account -> new AccountResponse()
                        .setId(account.getId())
                        .setCurrentBalance(account.getCurrentBalance()))
                .collect(toList());
    }


    @PostMapping("/transaction")
    public AccountResponse transaction(@RequestBody AccountRequest accountRequest) {
        transactionService.transaction(accountRequest.getId(), accountRequest.getSumOfTransaction());

        return accountRepository.findById(accountRequest.getId())
                .map(account -> new AccountResponse()
                        .setId(account.getId())
                        .setCurrentBalance(account.getCurrentBalance()))
                .get();

    }

    @GetMapping("/log")
    public List<TransactionResponse> getLogOfTransaction() {
        return logRepository.findAll()
                .stream()
                .map(transaction -> new TransactionResponse()
                        .setId(transaction.getId())
                        .setAccountId(transaction.getAccount().getId())
                        .setDate(transaction.getDate())
                        .setTransactionSum(transaction.getTransactionSum())).collect(toList());
    }
}
