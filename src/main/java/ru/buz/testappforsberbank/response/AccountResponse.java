package ru.buz.testappforsberbank.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
@Data
@Accessors(chain = true)
public class AccountResponse {
    private Long id;
    private BigDecimal currentBalance;
}
