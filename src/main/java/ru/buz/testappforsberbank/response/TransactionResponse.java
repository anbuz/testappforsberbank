package ru.buz.testappforsberbank.response;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.buz.testappforsberbank.db.models.Account;

import java.math.BigDecimal;
import java.util.Date;
@Data
@Accessors(chain = true)
public class TransactionResponse {
    private Long id;
    private Long accountId;
    private Date date;
    private BigDecimal transactionSum;
}
