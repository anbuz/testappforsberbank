package ru.buz.testappforsberbank.service;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentMatchers;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import ru.buz.testappforsberbank.db.models.Transaction;
import ru.buz.testappforsberbank.db.repository.AccountRepository;
import ru.buz.testappforsberbank.db.repository.TransactionLogRepository;
import ru.buz.testappforsberbank.request.AccountRequest;
import java.math.BigDecimal;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@SpringBootTest
@RunWith(SpringRunner.class)
public class TransactionServiceIntegrationTest {

    @Autowired
    private TransactionService transactionService;
    @MockBean
    private TransactionLogRepository transactionLogRepository;
    @Autowired
    private AccountRepository accountRepository;

    private AccountRequest accountRequest;


    @Before
    public void setUp() throws Exception {
        accountRequest = new AccountRequest();
        accountRequest.setId(1L).setSumOfTransaction(BigDecimal.valueOf(100));
    }

    @Test
    public void transaction() {
        assertEquals(BigDecimal.valueOf(555000.0), accountRepository.findById(1L).get().getCurrentBalance());

        transactionService.transaction(accountRequest.getId(),accountRequest.getSumOfTransaction());

        assertEquals(BigDecimal.valueOf(555100.0), accountRepository.findById(1L).get().getCurrentBalance());


    }

    @Test
    public void transactionLock() throws InterruptedException {
        when(transactionLogRepository.save(ArgumentMatchers.any())).thenAnswer(new Answer<Transaction>() {
            private boolean isFirstOne = true;
            @Override
            public Transaction answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (isFirstOne) {
                    Thread.sleep(10_000);
                    isFirstOne = false;
                }

                return new Transaction();
            }
        });

        new Thread(() -> {
            transactionService.transaction(accountRequest.getId(), accountRequest.getSumOfTransaction());
        }).start();

        Thread.sleep(2000);

        transactionService.transaction(accountRequest.getId(), accountRequest.getSumOfTransaction());

        assertEquals(BigDecimal.valueOf(555200.0), accountRepository.findById(1L).get().getCurrentBalance());
    }
}